﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NewWebProject.Startup))]
namespace NewWebProject
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
